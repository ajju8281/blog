'use strict';
var mongoose = require('../dbConnections/api_db');
var Schema = mongoose.Schema;

var blogSchema = new Schema({
    imageName:{
        type: String
    },
    username:{
        type: String
    },
    date:{
    },
    tittle:{
        type: String
    },
    discription:{
        type: String
    }
});

module.exports = mongoose.model('Blogs', blogSchema);
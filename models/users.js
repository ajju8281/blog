'use strict';
var mongoose = require('../dbConnections/api_db');
var Schema = mongoose.Schema;

var usersSchema = new Schema({
    email:{
        type: String,
        required: true,
        unique: true
    },
    username:{
        type: String,
        required: true,
        unique: true
    },
    password:{
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Users', usersSchema);

module.exports = {

    loginValidation: {
        "properties": {
            "username":{
                "type": "string",
                "required": true,
                "minLength": 4
            },
            "password": {
                "type": "string",
                "required": true,
                "minLength": 8
            }
        },
        "type": "object"
    },

    signupValidation: {
        "properties": {
            "username": {
                "type": "string",
                "required": true,
                "minLength": 4
            },
            "email": {
                "type": "string",
                "pattern": /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                "required": true
            },
            "password": {
                "type": "string",
                "required": true,
                "minLength": 8
            }
        },
        "type": "object"
    }
};
var apiMongoose = require('mongoose');


const dbUri = '';
//please add your mongodb url
apiMongoose.Promise = global.Promise;
apiMongoose.connect(dbUri, {
    useNewUrlParser: true
}, function (error) {
    if(error) {
        console.error("Error in connecting to API db",error);
    } else {
        console.log("Connected to  db");
    }
});

module.exports = apiMongoose;

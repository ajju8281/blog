var express = require('express');
var router = express.Router();
var JSONValidation = require('json-validation').JSONValidation;
var schemas = require('../helpers/schemaTypes');
var User= require('../models/users');
var Blog = require('../models/blog');
var jsonValidator = new JSONValidation();
var multer = require('multer');

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function(req, file, cb) {
        cb(null, ""+Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)+file.originalname);
    }

});

var upload = multer({
    storage: storage
});

/* GET home page. */
router.get('/', function(req, res, next) {
    res.sendfile('./public/addblog.html');
});

router.get('/data', function(req, res, next) {
    var currentPage=(parseInt(req.query.pageNo)-1)*5
    Blog.count({},function (err1,totalNoOfBlog) {
        if(totalNoOfBlog){
            Blog.find({},function (err,blog) {
                var data={
                    blog:blog,
                    totalNoOfBlog:Math.ceil(totalNoOfBlog/5)
                };
                res.send(data);
            }).sort({date:-1}).limit(6).skip(currentPage);
        }
    })
});

router.get('/singleblog', function(req, res, next) {
    Blog.findOne({_id:req.query.id},function (err,blog) {
        if(err){
            return;
        }
        res.send(blog);
    })
});

router.post('/login', function(req, res, next) {
    User.findOne({username:req.body.name,password:req.body.password1},function (err,result) {
        if(err) {
            res.status(500).json({
                "loginStatus":"fail"
            });
            return;
        }if(result){
            res.send({id:result._id,"loginStatus":"success"})
        }else{
            res.status(400).json({
                "loginStatus":"fail"
            });
        }
    });
});

router.route('/addblog').post(upload.any(),function (req, res) {
    const tokens = req.body.discription.split('\r\n\r\n');
    var discription= tokens.join('</p><p>')
    var imageLocation="uploads/";
    var discription= tokens.join('</p><p>')
    if(req.files[0].filename===undefined){
        imageLocation="uploads/"+req.files[0].filename;
    }
    User.findOne({_id:req.body.username},function (err,users) {
        if(users){
            var newBlog = new Blog({
                imageName: imageLocation,
                username: users.username,
                tittle: req.body.tittle,
                discription: discription,
                date: Math.floor(Date.now())
            });
            newBlog.save(function (err, userObject) {
                if (err) {
                    res.status(500).json({
                        error: "user already present please change username"
                    });
                    return;
                }
                res.redirect('/newblog?id='+userObject._id);

            });
        }

    })

});

router.get('/newblog',function(req,res){
    res.sendfile('./public/singleBlog.html');
});

router.post('/singup', function(req, res, next) {
    var result = jsonValidator.validate(req.body, schemas.signupValidation);
    if (!result.ok) {
        res.status(400).json({
            error: result
        });
        return;
    }
    User.findOne({username:req.body.username},function (err,result) {
        if(err) {
            res.status(500).json({
                error: err
            });
            return;
        }
        if(result) {
            res.status(400).json({
                error: "user already present please change username"
            });

        }else {
            var newUser=new User({
                username:req.body.username,
                password:req.body.password,
                email:req.body.email
            });
            newUser.save(function (err,userObject) {
                if(err){
                    res.status(500).json({
                        error: "user already present please change username"
                    });
                    return;
                }if(!userObject){
                    res.status(400).json({
                        error: "user registation in not done please try again"
                    });
                    return;
                }
                res.sendfile('./public/login.html');
            });
        }
    });
});

module.exports = router;

